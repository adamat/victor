# Victor - Makefile
build:conan.o loader.o gui.o persons.o notebook.o main.o common.o rules.o rparser.o
	g++ -rdynamic -o victor common.o conan.o main.o loader.o gui.o notebook.o persons.o rules.o rparser.o -LMAparser -lmaparser -ldl \
	`pkg-config --cflags --libs gtkmm-3.0 glibmm-2.4 libxml++-2.6`
gui.o:gui.cpp gui.h widgets.h notebook.o conan.o persons.o loader.o
	g++ -c -o gui.o gui.cpp `pkg-config --cflags --libs glibmm-2.4 gtkmm-3.0`
notebook.o:notebook.cpp notebook.h tabs.h
	g++ -c -o notebook.o notebook.cpp `pkg-config --cflags --libs glibmm-2.4 gtkmm-3.0`
persons.o:persons.cpp persons.h notebook.o gui.o
	g++ -c -o persons.o persons.cpp `pkg-config --cflags --libs glibmm-2.4 gtkmm-3.0`
conan.o:conan.cpp conan.h gui.o notebook.o
	g++ -c -o conan.o conan.cpp `pkg-config --cflags --libs glibmm-2.4`
common.o:common.h common.cpp
	g++ -c -o common.o common.cpp `pkg-config --cflags --libs glibmm-2.4`
loader.o:loader.cpp loader.h gui.o
	./make_modules.sh
	g++ -c -o loader.o loader.cpp -ldl `pkg-config --cflags --libs glibmm-2.4`
rules.o:rules.cpp rules.h
	g++ -c -o rules.o rules.cpp `pkg-config --cflags --libs glibmm-2.4`
rparser.o:rparser.cpp rparser.h
	g++ -c -o rparser.o rparser.cpp `pkg-config --cflags --libs libxml++-2.6 glibmm-2.4`
main.o:main.cpp gui.o
	g++ -c -o main.o main.cpp `pkg-config --cflags --libs gtkmm-3.0`
run: build
	./victor
