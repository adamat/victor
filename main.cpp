/*Victor - main file
  Only the main() function and some
  init functions could be here!
*/

#include "gui.h"
#include "widgets.h"
#include "loader.h"
#include "common.h"

int main (int argc, char **argv){
	Main kit(argc, argv); //Gtkmm initialisation
	
	if(InitGtk()){
		Err("Couldn't initialise gui, aborting!");
		return 1;
		}
	
	LoadDB();
	
	if(InitLoader()){
		Err("Couldn't initialise CONDOMS, aborting!");
		return 1;
		}

	kit.run(*wMain);
	
	return 0;
}
