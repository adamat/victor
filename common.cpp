/** Common functions
    Error and Warnings
**/
#include "common.h"
#include <iostream>

void Warn(ustring t){
	std::cerr << "Victor-Warning: " << t << '\n';
}

void Err(ustring t){
	std::cerr << "Victor-ERROR: " << t << '\n';
}


vector<ustring> explode(const ustring& in, const ustring& delim){
    const size_t delim_len = delim.length() ;
    vector<ustring> result ;
    size_t i = 0, j ;
    while(i<in.length()){
        j = in.find(delim, i) ;
        result.push_back(in.substr(i, j-i)) ;
        if (j == ustring::npos)
        break;
        i = j + delim_len ;
    }
    return result ;
}

