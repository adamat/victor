/*Victor - GUI file
  This file contains power code of stuff
  used for abstraction of the GTK+ GUI.
  Adam Matouŝek is babysitting this file.
*/

#include "notebook.h"
#include "tabs.h"
#include "gui.h"
#include "loader.h"
#include "conan.h"
#include "persons.h"
#include "common.h"

using Glib::RefPtr;
using Glib::ustring;
using Gdk::CONTROL_MASK;
using Gdk::SHIFT_MASK;
using sigc::mem_fun;
using sigc::ptr_fun;

Window* wMain = 0;
Dialog* wAddLocalTeam = 0, *wAddPerson = 0, *wAddPersons = 0, *wSelPersonsToTeam = 0;
ScrolledWindow* selperson_space = 0;
Container* tbspace = 0;
Container* mbspace = 0;
Notebook* ntbTeams = 0;
Widget* welcome_tab_content = 0;
RefPtr<ActionGroup> agMain;
RefPtr<Builder> GladeFile;
RefPtr<UIManager> ToolbarFile;
BeautifulColumnsOfDisciplinesWithAccesViaThese *columns = 0;
RefPtr<ListStore> persons_model;
TreeView *persons_list = 0;

RefPtr<FileFilter> db_file_filter;
ustring * FileName = 0;

BeautifulColumnsOfDisciplinesWithAccesViaThese::BeautifulColumnsOfDisciplinesWithAccesViaThese(){
	add(p_id); add(p_name);
	
	/*
	Add all disciplines
	*/
}

void SetIcon(){
	wMain->set_icon_name("victor");
	}

void GetWidgets(){
//	GladeFile->get_widget("", );
	GladeFile->get_widget("w_main", wMain);
	GladeFile->get_widget("tbspace", tbspace);
	GladeFile->get_widget("mbspace", mbspace);
	GladeFile->get_widget("ntb_teams", ntbTeams);
	GladeFile->get_widget("dialog_newlocalteam", wAddLocalTeam);
	GladeFile->get_widget("dialog_editperson", wAddPerson);
	GladeFile->get_widget("dialog_massperson", wAddPersons);
	GladeFile->get_widget("dialog_selpersons", wSelPersonsToTeam);
	GladeFile->get_widget("personslistspace", selperson_space);
	GladeFile->get_widget("l_welcome", welcome_tab_content);
}

void DefineActions(){
	agMain = ActionGroup::create();
//	agMain->add(Action::create(name,{ShowName|Stock::standard,[ForcedName]},help),AccelKey("K"), {mem|ptr}_fun());
	agMain->add(Action::create_with_icon_name("AddTeam", "tab-new", "Přidat _tým", "Přidá nový tým pro lokální potřeby"),AccelKey('T', CONTROL_MASK), ptr_fun(aAddTeam));
	agMain->add(Action::create("Save", Stock::SAVE), ptr_fun(aSave));
	agMain->add(Action::create("SaveAs", Stock::SAVE_AS),AccelKey('S', CONTROL_MASK | SHIFT_MASK), ptr_fun(aSaveAs));
	agMain->add(Action::create("Open", Stock::OPEN), ptr_fun(aOpen));
	agMain->add(Action::create("Quit", Stock::QUIT), mem_fun(*wMain, &Window::hide));
	agMain->add(Action::create("CreateOutputTeam", Stock::GO_FORWARD,"_Vytvořit výstupní tým", "Vytvoří tým z osob s nejlepšími výsledky"),AccelKey('T', CONTROL_MASK | SHIFT_MASK), ptr_fun(aCrtOutTm));
	agMain->add(Action::create_with_icon_name("Teamize", "media-playlist-shuffle", "_Rozřadit do týmů", "Zvolit, do kterých týmů\n které osoby patří"),AccelKey('G', CONTROL_MASK), ptr_fun(aTeamize));
	agMain->add(Action::create_with_icon_name("Summary", "config-users", "_Souhrn", "Otevře tab se všemi osobami"),AccelKey('F', CONTROL_MASK), ptr_fun(aSummary));
	agMain->add(Action::create_with_icon_name("AddPerson", "contact-new", "_Přidat Osobu", "Přidá novou nezařazenou osobu"),AccelKey('N', CONTROL_MASK | SHIFT_MASK), ptr_fun(aAddPerson));
	agMain->add(Action::create("AddPersons", "_Hromadné přidání osob", "Přidá nové nezařazené osoby"),AccelKey('M', CONTROL_MASK), ptr_fun(aAddPersons));
	agMain->add(Action::create("Settings", Stock::PROPERTIES), ptr_fun(aSettings));
	agMain->add(Action::create("About", Stock::ABOUT), ptr_fun(aAbout));
	agMain->add(Action::create("Manual", Stock::HELP), ptr_fun(aManual));
	//Menubar items
	agMain->add(Action::create("File", "_Soubor"));
	agMain->add(Action::create("Teams", "_Týmy"));
	agMain->add(Action::create("Persons", "_Osoby"));
	agMain->add(Action::create("Plugins", "Do_plňky"));
	agMain->add(Action::create("Help", "_Nápověda"));
}

short LoadToolbar(){
	ToolbarFile = UIManager::create();
	ToolbarFile->insert_action_group(agMain);
	wMain->add_accel_group(ToolbarFile->get_accel_group());
	try {
		ToolbarFile->add_ui_from_file("toolbar.xml");
	} catch(const Glib::Error& ex){
		Err(ex.what());
		return 1;
	}
    
	Widget* tb1 = ToolbarFile->get_widget("/mb_main");
	mbspace->add(*tb1);
	tb1 = ToolbarFile->get_widget("/tb_main");
	tb1->get_style_context()->add_class("primary-toolbar");
	tb1->reset_style();
	tbspace->add(*tb1);
	return 0;
}

short InitGtk(){
	GladeFile = Builder::create();
	
	try {
	GladeFile->add_from_file("victor.glade");
	} catch(const Glib::FileError& ex) {
		Err(ex.what());
		return 1;
	} catch(const Gtk::BuilderError& ex) {
		Err(ex.what());
		return 2;
	}
	GetWidgets();
	DefineActions();
	if(LoadToolbar()){
		Err("Unsuccessfull load of toolbar");
		return 4;
		}
	SetIcon();
	db_file_filter = FileFilter::create();
	db_file_filter->set_name("Databáze SQLite (.db)");
	db_file_filter->add_mime_type("application/x-sqlite");
	
	return 0;
}

void LoadDB(){
	/*PLACEHOLDER - Some openings, copying data to person storage, and creating list of disciplines*/
	columns = new BeautifulColumnsOfDisciplinesWithAccesViaThese();
	persons_model = ListStore::create(*columns);
	persons_model->set_sort_column(columns->p_name, SORT_ASCENDING);
	
	persons_list = new TreeView(persons_model);
	persons_list->append_column("Jméno", columns->p_name);
	persons_list->set_search_column(columns->p_name);
	persons_list->set_headers_visible(false);
	persons_list->set_visible(true);
	persons_list->get_selection()->set_mode(SELECTION_MULTIPLE);
	selperson_space->add(*persons_list);
}

void UnloadDB(){
	delete persons_list;
	persons_model.reset();
	delete columns;
}

void msgNotImplemented(){
	gkErr("Tato funkce ještě nebyla implementována,\npravděpodobně kvůli mizernému financování.", "Victor právě unikl SIGSEGVu");
}

void gkErr(ustring t, ustring h){
	
	MessageDialog d(*wMain, h, false, MESSAGE_ERROR);
	d.set_secondary_text(t, true);
	d.run();
}

void gkWarn(ustring t, ustring h){
	
	MessageDialog d(*wMain, h, false, MESSAGE_WARNING);
	d.set_secondary_text(t, true);
	d.run();
}
unsigned menuAddTeam(const string& name){
	agMain->add(Action::create(name, name), sigc::bind(sigc::ptr_fun(AddLocalTab), name));
	UIManager::ui_merge_id id = ToolbarFile->new_merge_id();
	ToolbarFile->add_ui(id, "/mb_main/Teams/TeamList/", name, name);
	ToolbarFile->ensure_update();
	return (unsigned)id;
}

void menuDeleteTeam(const string& name){
	agMain->remove(agMain->get_action(name));
	ToolbarFile->remove_ui(local_teams[name]->first);
}

void menuAddModule(int mod){
	agMain->add(Action::create((modules.at(mod)).getdirname(), (modules.at(mod)).getname()), sigc::mem_fun(modules.at(mod), &module::load));
	UIManager::ui_merge_id id = ToolbarFile->new_merge_id();
	ToolbarFile->add_ui(id, "/mb_main/Plugins/plugin-list/", (modules.at(mod)).getdirname(), (modules.at(mod)).getdirname());
}

/*local_team_container_t::iterator GetCurrentTeam(){
	Warn(ntbTeams->get_tab_label_text(*ntbTeams->get_nth_page(ntbTeams->get_current_page())).c_str());
	local_team_container_t::iterator i = local_teams.find(ntbTeams->get_tab_label_text(*ntbTeams->get_nth_page(ntbTeams->get_current_page())).c_str());
	return i;
}*/

void Save(){
	if(!FileName) SaveAs();
	else{
		/*Save*/
	}
}

void SaveAs(){
	FileChooserDialog fcd(*wMain, "Zvolte soubor k uložení databáze", FILE_CHOOSER_ACTION_SAVE);
	fcd.set_do_overwrite_confirmation();
	fcd.add_filter(db_file_filter);
	fcd.run();
	
}

void AddLocalTeam(){
	Entry* e;
	GladeFile->get_widget("entry_name_newlocalteam", e);
	e->grab_focus();
	e->set_text("");
	if (wAddLocalTeam->run() == 1) CreateLocalTeam(e->get_text());
	wAddLocalTeam->hide();
}

void base_AddPerson(const char* add_to_team){ // add to current team
	Entry *fn, *ln, *cnt, *brt;
	GladeFile->get_widget("entry_name_newperson", fn);
	GladeFile->get_widget("entry_lname_newperson", ln);
	GladeFile->get_widget("entry_contact_newperson", cnt);
	GladeFile->get_widget("calendar_newperson", brt);
	fn->grab_focus();
	fn->set_text("");
	ln->set_text("");
	cnt->set_text("");
	unsigned p;
	Glib::Date d;
	switch(wAddPerson->run()){
		case 2:
			d.set_parse(brt->get_text());
			p = storage.AddPerson(fn->get_text(), ln->get_text(), cnt->get_text(), d);
			if(add_to_team) AddPersonToTeam(add_to_team, p);
			Warn("Persona přidána");
		case RESPONSE_CANCEL:
		default: break;
	}//switch
	wAddPerson->hide();
}

void AddPerson(){ base_AddPerson(0);}
void AddPersonDirectlyToTeam(const char* name){ base_AddPerson(name);}

void AddPersons(){
	Entry *fn, *ln, *cnt, *brt;
	GladeFile->get_widget("entry_name_newperson1", fn);
	GladeFile->get_widget("entry_lname_newperson1", ln);
	GladeFile->get_widget("entry_contact_newperson1", cnt);
	GladeFile->get_widget("calendar_newperson1", brt);
	int res;
	Glib::Date d;
	do{
		fn->grab_focus();
		fn->set_text("");
		ln->set_text("");
		cnt->set_text("");
		brt->set_text("");
		res = wAddPersons->run();
		switch(res){
			case -2:
			case  2:
				d.set_parse(brt->get_text());
				storage.AddPerson(fn->get_text(), ln->get_text(), cnt->get_text(), d);
			case RESPONSE_CANCEL:
			default: break;
		}//switch
	} while (res == -2);
	wAddPersons->hide();
}

void SelectPersonsToTeam(const char* name){
	Warn("Popřiřazují se osoby. Tým:");
	Warn(name);
	persons_list->get_selection()->unselect_all();
	vector<TreePath> rows;
	switch(wSelPersonsToTeam->run()){
		case 2: //OK
			rows = persons_list->get_selection()->get_selected_rows();
			for(vector<TreePath>::iterator i = rows.begin(); i != rows.end(); i++){
				AddPersonToTeam(name, persons_model->get_iter(*i)->get_value(columns->p_id));
				Warn(persons_model->get_iter(*i)->get_value(columns->p_name).c_str());
			}
		default:
			wSelPersonsToTeam->hide();
			break;
	}
}

void EditPerson(unsigned p){
	Person* psn;
	try{
		psn = &storage.GetPerson(p);
	}catch (bool){
		Err("Persona neexistuje");
		return;
	}
	Entry *fn, *ln, *cnt, *brt;
	GladeFile->get_widget("entry_name_newperson", fn);
	GladeFile->get_widget("entry_lname_newperson", ln);
	GladeFile->get_widget("entry_contact_newperson", cnt);
	GladeFile->get_widget("calendar_newperson", brt);
	fn->grab_focus();
	fn->set_text(psn->GetFirstName());
	ln->set_text(psn->GetLastName());
	cnt->set_text(psn->GetContact());
	brt->set_text(psn->GetBirth().format_string("%x"));
	Glib::Date d;
	switch(wAddPerson->run()){
		case 2:
			d.set_parse(brt->get_text());
			Warn("Přiřadím jméno a příjmení");
			psn->SetFirstName(fn->get_text());
			psn->SetLastName(ln->get_text());
			Warn("Přiřadím kontakt a datum");
			psn->SetContact(cnt->get_text());
			psn->SetBirthday(d);
			Warn("Persona upravena");
		case RESPONSE_CANCEL:
		default: break;
		}//switch
	wAddPerson->hide();
}
