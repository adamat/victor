#ifndef MAPARSER_H
#define MAPARSER_H 1

#include <boost/variant/static_visitor.hpp>
#include <boost/variant/variant.hpp>
#include <boost/variant/recursive_variant.hpp>

#include <glibmm/ustring.h>
#include <string>
#include <vector>

namespace MAparser
{
    class mTree;
    
    using Glib::ustring;
    typedef boost::variant <double, boost::recursive_wrapper<mTree> > mTree_node;
    typedef double (*fFunc)(std::vector<double>&);

    class mTree
    {
        public:
            double (*oFunc)(std::vector<double>&);
            std::vector<mTree_node> operands;
            
            double count();
    };
    
    struct mTreeCount
    {
        double operator()(mTree const& tree) const;
    };

    struct mTree_nodeCount : boost::static_visitor<double>
    {
        double operator()(mTree const& tree) const;
        double operator()(double const& num) const;
    };

    double fBase(std::vector<double>& vect);
    
    double fPlus(std::vector<double>& vect);
    double fMinus(std::vector<double>& vect);
    double fKrat(std::vector<double>& vect);
    double fDel(std::vector<double>& vect);
    double fVykon(std::vector<double>& vect);
    double fPower(std::vector<double>& vect);
    double fSqrt(std::vector<double>& vect);
    
    mTree mParse(const ustring& ex_string) throw(bool);
}

#endif
