#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant/static_visitor.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <string>
#include <vector>

#include "maparser.h"

namespace MAparser
{
    namespace fusion = boost::fusion;
    namespace phoenix = boost::phoenix;
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;
    
    
    double mTree::count(){
        std::vector<double> vect;
        BOOST_FOREACH(mTree_node const& operand, operands){
            double a=boost::apply_visitor(mTree_nodeCount(), operand);
            vect.push_back(a);
        }
        return oFunc(vect);
    }
    
    double mTreeCount::operator()(mTree const& tree) const{
        std::vector<double> vect;
        BOOST_FOREACH(mTree_node const& operand, tree.operands){
            double a=boost::apply_visitor(mTree_nodeCount(), operand);
            vect.push_back(a);
        }
        return tree.oFunc(vect);
    }
    
    double mTree_nodeCount::operator()(mTree const& tree) const{
        return mTreeCount()(tree);
    }
    double mTree_nodeCount::operator()(double const& num) const{
        return num;
    }

}

BOOST_FUSION_ADAPT_STRUCT(
    MAparser::mTree,
    (MAparser::fFunc, oFunc)
    (std::vector<MAparser::mTree_node>, operands)
)
   
namespace MAparser{   
    double fBase(std::vector<double>& vect){return vect.front();}
    
    double fPlus(std::vector<double>& vect){
	    return vect.at(0)+vect.at(1);
    }
    double fMinus(std::vector<double>& vect){
	    return vect.at(0)-vect.at(1);
    }
    double fKrat(std::vector<double>& vect){
	    return vect.at(0)*vect.at(1);
    }
    double fDel(std::vector<double>& vect){
	    return vect.at(0)/vect.at(1);
    }
    double fVykon(std::vector<double>& vect){
	    return 1;
    }
    double fPower(std::vector<double>& vect){
	    return pow(vect.at(0),vect.at(1));
    }
    double fSqrt(std::vector<double>& vect){
	    return sqrt(vect.at(0));
    }
    struct functionsList : qi::symbols<char,fFunc>
    {
        functionsList();
    } functionsList_;
    
    struct math_tree_parser : qi::grammar<ustring::const_iterator, mTree(), ascii::space_type>
    {
        qi::rule<ustring::const_iterator,fFunc(), ascii::space_type> plus;
	    qi::rule<ustring::const_iterator,fFunc(), ascii::space_type> minus;
	    qi::rule<ustring::const_iterator,fFunc(), ascii::space_type> krat;
	    qi::rule<ustring::const_iterator,fFunc(), ascii::space_type> del;
	    qi::rule<ustring::const_iterator, mTree(), ascii::space_type> functions_;
	    qi::rule<ustring::const_iterator, mTree(), ascii::space_type> vyraz_1;
	    qi::rule<ustring::const_iterator, mTree(), ascii::space_type> vyraz_2;
	    qi::rule<ustring::const_iterator, mTree_node(), ascii::space_type> vyraz_3;
	    
        math_tree_parser();
    };
    
    functionsList::functionsList(){
        add
            ("x",&fVykon)
            ("POWER",&fPower)
            ("SQRT",&fSqrt)
        ;
    }

    math_tree_parser::math_tree_parser() : math_tree_parser::base_type(vyraz_1,"polynom"){
        using ascii::char_;
        using qi::eps;
        using qi::double_;
        using qi::on_error;
        using qi::fail;
        using namespace qi::labels;
        using phoenix::at_c;
        using phoenix::push_back;
        using phoenix::assign;
        using phoenix::front;
        using phoenix::val;
        using phoenix::construct;
        
        plus=char_('+')[_val=&fPlus];
        minus=char_('-')[_val=&fMinus];
        krat=char_('*')[_val=&fKrat];
        del=char_('/')[_val=&fDel];
        
        functions_ =
            functionsList_[at_c<0>(_val)=_1]
            > -(
                char_('(')
                > vyraz_1[push_back(at_c<1>(_val),front(at_c<1>(_1)))] % char_(',')
                > char_(')')
            )
        ;
        
        vyraz_3=
            double_[_val=_1]
            | functions_[_val=_1]
            | (
                char_('(')
                > vyraz_1[_val=front(at_c<1>(_1))]
                > char_(')')
            )
        ;
        
        vyraz_2=
            vyraz_3[push_back(at_c<1>(_val),_1)]
            > *(
                (krat|del)[at_c<0>(_val)=_1]
                > vyraz_3[push_back(at_c<1>(_val),_1)]
                >> eps[assign(at_c<1>(_val),1,_val)]
                >> eps[at_c<0>(_val)=&fBase]
            )
        ;
        
        vyraz_1=
            vyraz_2[push_back(at_c<1>(_val),front(at_c<1>(_1)))]
            > *(
                (plus|minus)[at_c<0>(_val)=_1]
                > vyraz_2[push_back(at_c<1>(_val),front(at_c<1>(_1)))]
                >> eps[assign(at_c<1>(_val),1,_val)]
                >> eps[at_c<0>(_val)=&fBase]
            )
        ;
        
        vyraz_1.name("polynom");
        vyraz_2.name("nom");
        vyraz_3.name("konst");
        functions_.name("function");
        plus.name("+");
        minus.name("-");
        krat.name("*");
        del.name("/");
        functionsList_.name("funcName");
        
        on_error<fail>
        (
            vyraz_1
          , std::cout
                << val("Error! Expecting ")
                << _4                               // what failed?
                << val(" here: \"")
                << construct<std::string>(_3, _2)   // iterators to error-pos, end
                << val("\"")
                << std::endl
        );
    }
    
    mTree mParse(const ustring& ex_string) throw(bool){
        math_tree_parser parser;
        ustring::const_iterator iter=ex_string.begin(),end=ex_string.end();
        mTree returned;
        bool check=qi::phrase_parse(iter, end, parser, ascii::space, returned);
        if(!check || iter != end)throw true;
        return returned;
    }
}
