#include "rules.h"
#include "common.h"

using namespace std;
using Glib::ustring;

Base_rule::Base_rule(bool s, unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx){
	max_starting_persons=msp;
	birth_min=bmn;
	birth_max=bmn;
	class_min=cmn;
	class_max=cmx;
	sex=s;
}

Discipline::Discipline(ustring dc, mTree pe, unsigned short mcp, bool s, unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx):Base_rule(s,msp,bmn,bmx,cmn,cmx){
	Discipline_code=dc;
	point_expression=pe;
	max_counted_persons=mcp;
}
ustring Discipline::GetCode(){
    return Discipline_code;
}

Rule::Rule(bool s, mTree ae,unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx):Base_rule(s,msp,bmn,bmx,cmn,cmx){
	add_expression=ae;
	Disciplines=vector<Discipline>();
	persons=vector<Person*>();
}
void Rule::addDiscipline(Discipline disc){
    Disciplines.push_back(disc);
}
Person* Rule::addPerson(Person* per){
    persons.push_back(per);
    return persons.at(persons.size());
}
