#ifndef _COMMON_H_
#define _COMMON_H_ 0

#include <glibmm/ustring.h>
#include <vector>

using Glib::ustring;
using std::vector;

void Warn(ustring);
void Err(ustring);

vector<ustring> explode(const ustring& in, const ustring& delim);

template <typename T>
void numConvert(T& t, const ustring& s, std::ios_base& (*f)(std::ios_base&)) throw(bool){
  std::istringstream truc(s);
  if((truc >> f >> t).fail()) throw true;
}

template <typename T>
ustring toStr(const T& t) throw(bool){
    ustring ret;
    std::ostringstream truc(ret);
    if((truc<<ret).fail()) throw true;
    return ret;
}
#endif
