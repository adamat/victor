//loader.h
#ifndef _LOADERHEADER_H_
#define _LOADERHEADER_H_ 0

#include <dlfcn.h> // dlopen()...
#include <cstdlib> // system()
#include <glibmm/ustring.h>
#include <vector>

using namespace Glib;
using namespace std;

int InitLoader();

class module
 {
	private:
		ustring directory_name;
		ustring module_name;
		ustring module_info;
		bool module_type;
	
	public:
		module();
		module(const ustring& directory_name_temp, const bool& module_type_temp, const ustring& module_name_temp, const ustring& module_info_temp);
		~module();
		void load();	
		const ustring& getname();
		const ustring& getdirname();
		const ustring& getinfo();
		const bool& gettype();
 };

extern vector <module> modules;

#endif
