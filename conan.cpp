/**CONAN - CONnect ActioN**/

#include "conan.h"
#include "gui.h"
#include "notebook.h"


void (*aAddTeam)	() = AddLocalTeam;
void (*aSave)		() = Save;
void (*aSaveAs)		() = SaveAs;
void (*aOpen)		() = msgNotImplemented;
void (*aQuit)		() = msgNotImplemented;
void (*aCrtOutTm)	() = AddOutputTab;
void (*aTeamize)	() = AddTestTab;
void (*aSummary)	() = AddSummaryTab;
void (*aAddPersons)	() = AddPersons;
void (*aAddPerson)	() = AddPerson;
void (*aSettings)	() = msgNotImplemented;
void (*aAbout)		() = msgNotImplemented;
void (*aManual)		() = msgNotImplemented;
