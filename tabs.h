/** Tabs' classes - prototypes
	Adam Matouŝek is babysitting
	this file
**/

#ifndef _TABS_H_
#define _TABS_H_ 0

#include "widgets.h"
#include <map>
#include <set>

using namespace Glib;
using namespace std;

class BaseTab{
	private:
		Box* label_box;
		Button* b_close;
		Image* i_close;
		Label* l_label;
	protected:
		Widget* content;
		ustring* label;
		void show_tab();	//Should be a part of the constructor
	public:
		virtual ~BaseTab();
		void self_remove();
		void expose();		//Grabs focus
};

class TestTab : public BaseTab{
	public:
		TestTab();
		~TestTab();
};

class LocalTab : public BaseTab{
		RefPtr<Builder> tab_file;
		void rename_from_entry();
		// List of persons
		TreeView *tview;
		RefPtr<TreeModelFilter> filter;
		bool filter_func(const TreeModel::const_iterator& row);
		void remove_person(const TreeModel::iterator& i);
		void button_remove_persons();
		void edit_person();
	public:
		LocalTab(const ustring& name);
		~LocalTab();
		void Refilter();
		/* Signal Handlers */
};

class SummaryTab : public BaseTab{
		RefPtr<Builder> tab_file;
		TreeView *tview;
		void remove_person(const TreeModel::iterator& i);
		void button_remove_persons();
		void edit_person();
	public:
		SummaryTab();
		~SummaryTab();
};

class OutputTab : public BaseTab{
		RefPtr<Builder> tab_file;
	public:
		OutputTab();
		~OutputTab();
		/* Signal Handlers */
};

extern set <BaseTab*> tabs;
extern map <string, LocalTab*> local_tabs;

#endif
