/**CONAN - CONnect ActioN**/

#ifndef _CONAN_H_
#define _CONAN_H_
//Function pointers to be connected with actions ("Aliases")
extern void (*aAddTeam)		();
extern void (*aSave)			();
extern void (*aSaveAs)			();
extern void (*aOpen)			();
extern void (*aQuit)			();
extern void (*aCrtOutTm)		();
extern void (*aTeamize)		();
extern void (*aSummary)		();
extern void (*aAddPersons)		();
extern void (*aAddPerson)		();
extern void (*aSettings)		();
extern void (*aAbout)			();
extern void (*aManual)			();
#endif
