/*Victor - GUI file
  This file contains prototypes of stuff
  used for abstraction of the GTK+ GUI.
  Adam Matouŝek is babysitting this file.
*/
#ifndef _GUI_H_
#define _GUI_H_ 0


#include <string>
#include <glibmm/ustring.h>

using namespace std;
using Glib::ustring;

short InitGtk();

void LoadDB(/*Probably some file identifier*/);
void UnloadDB();

void msgNotImplemented();

//ALL-PURPOSE
void gkErr(ustring, ustring = "Nastal problém");	//("main text", "Header");
void gkWarn(ustring, ustring = "Varování!");	//("main text", "Header");

//MENUS
unsigned menuAddTeam(const string& name);
void menuDeleteTeam(const string& name);
void menuAddModule(int mod);

//DIALOGS
void Save();
void SaveAs();

void AddLocalTeam();
void AddPerson();
void AddPersonDirectlyToTeam(const char* name);
void AddPersons();
void SelectPersonsToTeam(const char* name);
void EditPerson(unsigned p);

#endif
