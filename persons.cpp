/** Persons' Storage data,
	managing persons.
	Adam Matouŝek is babysitting
	this file
**/

#include "persons.h"
#include "notebook.h"
#include "gui.h"
#include "common.h"
#include "widgets.h"
#include "tabs.h"

using namespace std;

bool StartTab = 1;

/********** Person ************/
Person::Person():noneDisc(ustring("0000"),mTree(),0,0,0,0,0,0,0), is_tmp(1){;}
Person::Person(unsigned id, const ustring &fname, const ustring &lname, const ustring &contact, Glib::Date sbirth)
			:noneDisc(ustring("0000"),mTree(),0,0,0,0,0,0,0), is_tmp(0){

			misa = *persons_model->append();
			misa.set_value(columns->p_id, id);
			SetFirstName(fname);
			SetLastName(lname);
			SetContact(contact);
			SetBirthday(sbirth);
}

bool Person::invalid(){
	if(misa && !is_tmp) return 0;
	Err("Nikdy si nezahrávej s neplatným iterátorem!");
	return 1;
}

void Person::erase_row(){
	Warn("Vymažu svůj řádek");
	if(!invalid())
		persons_model->erase(misa);
}
Person::~Person(){
	//if(!invalid()) erase_row();
}

void Person::update_name(){
	if(invalid()) return;
	ustring both_names = first_name;
	both_names += " ";
	both_names += last_name;
	Warn("Aplikuji nové jméno:");
	Warn(both_names.c_str());
	misa.set_value(columns->p_name, both_names);
}

void Person::SetFirstName(ustring n){
	if(invalid()) return;
	Warn("Nejsem invalida, poměním");
	first_name = n;
	Warn("Update jména");
	update_name();
	Warn("Done");
	}
void Person::SetLastName(ustring n){
	if(invalid()) return;
	last_name = n;
	update_name();
	}
	
void Person::SetContact(ustring n){if(invalid()) return; contact = n;}
void Person::SetBirthday(Glib::Date sbirth){
	if(invalid()) return;
	birth=sbirth;
}

const ustring& Person::GetFirstName()const{return first_name;}
const ustring& Person::GetLastName()const{return last_name;}
const ustring& Person::GetContact()const{return contact;}
Glib::Date Person::GetBirth()const{return birth;}
ustring Person::GetChngSQL(){
    ustring ret;
    for(map<Discipline*, TreeModelColumn<double> >::iterator iter=columns->p_record.begin(); iter!=columns->p_record.end(); iter++)
        ret += iter->first->GetCode() + "=" + toStr(misa.get_value(columns->p_record[iter->first])) + " ";
    return ret;
}
ustring Person::GetInsSQL(){
    ustring ret="";
    for(map<Discipline*, TreeModelColumn<double> >::iterator iter=columns->p_record.begin(); iter!=columns->p_record.end(); iter++)
        ret += toStr(misa.get_value(columns->p_record[iter->first]))+",";
    ret.erase(ret.size()-1);
    ret.append(")");
    return ret;
}
/*double Person::operator[](Discipline *d){
	if(!rec.count(d))rec[d] = 0;//If "d" doesn't exist in map, assign it 0
	return &rec[d];
}*/

/*********** GLOBAL ***********/
person_container_t::person_container_t():map(),pid(0){;}

unsigned int person_container_t::AddPerson(const ustring &fname, const ustring &lname, const ustring &contact, Glib::Date sbirth){
	Warn("Potvořím personu");
    insert(pair<unsigned int, pair<Person, bool> >(pid, pair<Person, bool>(Person(pid, fname, lname, contact, sbirth), true) ) );
    pid++;
    return pid-1;
}
unsigned int person_container_t::NextID(){ return pid;}
void person_container_t::DelPerson(unsigned int id)throw(bool){
    if(count(id)==0)throw true;
    if(map<unsigned int, pair<Person, bool> >::operator[](id).second==false){
        chnglog+="DELETE FROM Global WHERE ID="+toStr(id)+"; ";
    }
    find(id)->second.first.erase_row();
    for(local_team_container_t::iterator i = local_teams.begin(); i != local_teams.end(); i++)
    	i->second->second.erase(id);
    erase(id);
}
Person& person_container_t::GetPerson(unsigned int id)throw(bool){
    if(count(id)==0)throw true;
    return map<unsigned int, pair<Person, bool> >::operator[](id).first;
}
ustring person_container_t::GetSQL(){
    ustring ins;
    for(iterator iter=begin(); iter!=end(); iter++){
        if(iter->second.second==0){
            chnglog+="UPDATE Global SET "+iter->second.first.GetChngSQL()+" WHERE ID="+toStr(iter->first)+"; ";
        }
        else{
            chnglog+="INSERT INTO Global VALUES ("+toStr(iter->first)+","+iter->second.first.GetInsSQL()+"; ";
        }
    }
    
}

person_container_t storage;
local_team_container_t local_teams;


void CreateLocalTeam(const ustring& name, const set<unsigned int> & from){
	if(!local_teams.count(name.c_str())){
			local_teams[name.c_str()] = new local_team_data_t(menuAddTeam(name.c_str()), from);
	}
	AddLocalTab(name);
}

void DeleteLocalTeam(const ustring& name){
	menuDeleteTeam(name.c_str());
	local_teams.erase(name.c_str());
	CloseLocalTab(name.c_str());
}

void RenameLocalTeam(const ustring& team, const ustring& new_name){
	CreateLocalTeam(new_name, local_teams[team.c_str()]->second);
	DeleteLocalTeam(team);
}

void AddPersonToTeam(const char* team, unsigned int person){
	local_team_container_t::iterator i = local_teams.find(team);
	if(i != local_teams.end()){
		Warn("Spouštím výkonné přiřazovací jádro");
		i->second->second.insert(person);
		if(local_tabs.count(team)) local_tabs[team]->Refilter();
	}
}

void RemovePersonFromTeam(const char* team, unsigned int person){
	local_team_container_t::iterator i = local_teams.find(team);
	if(i != local_teams.end()){
		local_teams[team]->second.erase(person);
		if(local_tabs.count(team)) local_tabs[team]->Refilter();
	}
}
