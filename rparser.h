#ifndef RPARSER_H_
#define RPARSER_H_ 0
#include <map>
#include <vector>
#include <string>
#include <glibmm/ustring.h>
#include <libxml++/libxml++.h>

#include "rules.h"

class Rule_sax : public xmlpp::SaxParser{
    private:
        std::vector<Rule> returned;
        std::vector<Glib::ustring> names;
        std::vector<AttributeList> attributes;
        enum senum{
            rule,
            discipline,
            style,
            code,
            add_expression,
            sex,
            max_starting_persons,
            max_counted_persons,
            birth_min,
            birth_max,
            class_min,
            class_max,
            point_expression
        };
        std::map<std::string,senum> mapForSwitch;
  
    protected:
        virtual void on_start_document();
        virtual void on_end_document();
        virtual void on_start_element(const Glib::ustring& name, const AttributeList& properties);
        virtual void on_end_element(const Glib::ustring& name);
        virtual void on_characters(const Glib::ustring& characters);
        virtual void on_comment(const Glib::ustring& text);
        virtual void on_warning(const Glib::ustring& text);
        virtual void on_error(const Glib::ustring& text);
        virtual void on_fatal_error(const Glib::ustring& text);
        
    public:
        Rule_sax();
        virtual ~Rule_sax();
    
};

std::vector<Rule> makeRules(Glib::ustring path);
#endif
