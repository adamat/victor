/** Persons' Storage data,
	managing persons, local teams.
	Adam Matouŝek is babysitting
	this file
**/

#ifndef _PERSONS_H_
#define _PERSONS_H_ 0

#include "common.h"
#include "rules.h"
#include <glibmm/ustring.h>
#include <glibmm/date.h>
#include <gtkmm/treeiter.h>
#include <map>
#include <set>

using Glib::ustring;
using Gtk::TreeRow;
using namespace std;

class Person{
	private:
		ustring first_name, last_name, contact;
		Gtk::TreeRow misa;
		Glib::Date birth;
		Discipline noneDisc;
		void update_name();
		bool invalid();
		bool is_tmp;
		//map <Discipline*, double> rec;
	public:
		Person();
		Person(unsigned id, const ustring &fname, const ustring &lname, const ustring &contact, Glib::Date sbirth);
		~Person();
		void erase_row();
		void SetFirstName(ustring n);
		void SetLastName(ustring n);
		void SetContact(ustring n);
		void SetBirthday(Glib::Date sbirth);
		
		const ustring& GetFirstName()const;
		const ustring& GetLastName()const;
		const ustring& GetContact()const;
		Glib::Date GetBirth()const;
		ustring GetChngSQL();
		ustring GetInsSQL();
		//double operator[](Discipline* d);
		/*bool IsFreeDisc(Discipline* d);*/
};

class person_container_t : protected map<unsigned int, pair<Person, bool> >{
    private:
        ustring chnglog;
        long pid;
    public:
        person_container_t();
        unsigned int AddPerson(const ustring &fname, const ustring &lname, const ustring &contact, Glib::Date sbirth);
        unsigned int NextID();
        void    DelPerson(unsigned int id)throw(bool);
        Person& GetPerson(unsigned int id)throw(bool);
	    ustring GetSQL();
};

typedef pair<unsigned int, set<unsigned int> > local_team_data_t;
typedef map<string, local_team_data_t*> local_team_container_t;

extern person_container_t storage;
extern local_team_container_t local_teams;

void CreateLocalTeam(const ustring& name, const set<unsigned int> & from = set<unsigned int>());
void DeleteLocalTeam(const ustring& name);
void RenameLocalTeam(const ustring& team, const ustring& new_name);

void AddPersonToTeam(const char* team, unsigned int person);
void RemovePersonFromTeam(const char* team, unsigned int person);

#endif
