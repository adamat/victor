/** Funcs for managing tabs
	Adam Matouŝek is babysitting
	this file
**/

#ifndef _NOTEBOOK_H_
#define _NOTEBOOK_H_ 0

#include <glibmm/ustring.h>

using Glib::ustring;

void AddTestTab();
void AddLocalTab(const ustring& name);
void CloseLocalTab(const char* name);
void AddSummaryTab();
void AddOutputTab();
#endif
