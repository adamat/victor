/*Victor - Widget file
  This file contains public gtk widgets.
  Adam Matouŝek is babysitting this file.
*/

#ifndef _WIDGETS_H_
#define _WIDGETS_H_ 0

#include <gtkmm.h>
#include "rules.h"
#include <map>

using namespace Gtk;

extern Window* wMain;
extern Dialog *wAddLocalTeam, *wAddPerson, *wAddPersons;
extern Notebook* ntbTeams;
extern Widget* welcome_tab_content;

class BeautifulColumnsOfDisciplinesWithAccesViaThese: public TreeModelColumnRecord{
	public:
		BeautifulColumnsOfDisciplinesWithAccesViaThese();
		
		TreeModelColumn <unsigned> p_id;
		TreeModelColumn <Glib::ustring/***/ > p_name;
		std::map<Discipline*, TreeModelColumn<double> > p_record;
};
extern BeautifulColumnsOfDisciplinesWithAccesViaThese *columns;
extern Glib::RefPtr<ListStore> persons_model;

#endif
