#ifndef RULES_H_
#define RULES_H_ 0
//#include <boost/tribool.hpp>
#include <map>
#include <vector>
#include <string>
#include <glibmm/ustring.h>
#include "MAparser/maparser.h"

class Person;

using MAparser::mTree;
using MAparser::mParse;

class Base_rule{
	protected:
		unsigned short max_starting_persons;
		unsigned short birth_min, birth_max;
		unsigned short class_min, class_max;
		bool sex;
		
	public:
		Base_rule(bool s,unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx);
};

class Discipline : protected Base_rule{
	protected:
		Glib::ustring Discipline_code;
		mTree point_expression;
		unsigned short max_counted_persons;
		std::vector<Person*> persons;
		
	public:
		Discipline(Glib::ustring dc, mTree pe,unsigned short mcp, bool s, unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx);
		Glib::ustring GetCode();
};

class Rule : protected Base_rule{
	protected:
		std::vector<Discipline> Disciplines;
		std::vector<Person*> persons;
		mTree add_expression;
		
	public:
		Rule(bool s, mTree ae,unsigned short msp, unsigned short bmn, unsigned short bmx, unsigned short cmn, unsigned short cmx);
		void addDiscipline(Discipline disc);
		Person* addPerson(Person* pers);
};

#endif
