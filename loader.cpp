// loader.cpp
#include "gui.h"
#include "loader.h"
#include <fstream>

vector <module> modules;
typedef void (*loaded_function)();
int number=152;	//----- kvůli pokusnému modulu

int InitLoader()
{	
	int iTemp = 0;
	ustring uTemp;
	
	system("ls modules > modulelist");	//rewrite last output
	modules.erase(modules.begin(),modules.end());	//delete vector modules if any

	ifstream modl("modulelist");
	if (!modl.is_open()) // can't open modulelist
		return 1;
	
	while (modl >> uTemp)
	 {
		string sTemp = "modules/" + uTemp + "/info";
		bool type;
		string name;
		string info;
		
		ifstream ifs(sTemp.c_str());
		if (!ifs.is_open()) // can't open info file
			continue;
		
		ifs>>type;
		if (!ifs)
		 {	type=false;
			ifs.clear();
		 }	
		else
			ifs.get();
		
		getline(ifs, name);
		if ((name.size())==0)
			name=uTemp; //default name
		
		while ( getline(ifs, sTemp) )
			info += sTemp +='\n';
		if ((info.size())==0)
			info="Žádné informace nejsou k dispozici"; //default info
		
		modules.push_back(module(uTemp, type, name, info));

		ifs.close();		
		iTemp++;
	 }
	modl.close();
	while (iTemp--)
		menuAddModule(iTemp);
	return 0;
}

//******************************member functions:

module::module(const ustring& directory_name_temp, const bool& module_type_temp, const ustring& module_name_temp, const ustring& module_info_temp)
{
	directory_name = directory_name_temp;
	module_type = module_type_temp;
	module_name = module_name_temp;
	module_info = module_info_temp;
}

module::~module()
{}

void module::load()
{
	ustring uTemp = "modules/" + directory_name + "/library.so";
	
	void* lib_handle = dlopen(uTemp.c_str(), RTLD_LAZY);

		if (!lib_handle) // library wasn't found
		 {
			gkErr("Tento modul se nepodařilo najít");
			return;
		 }
	if (module_type == false)
		gkWarn("Varování, daný modul je potenciálně nebezpečný a může měnit vaše data");

	loaded_function func = (loaded_function) dlsym(lib_handle, "module");
	
		if (dlerror()) // function wasn't found
		 {
			gkErr("Tento modul se nepodařilo spustit");
			dlclose(lib_handle);
			return;
		 }

	func();

	dlclose(lib_handle);
	return;
}

const ustring& module::getname()
{
	return module_name;
}

const ustring& module::getdirname()
{
	return directory_name;
}

const ustring& module::getinfo()
{
	return module_info;
}

const bool& module::gettype()
{
	return module_type;
}



