#include <iostream>
#include <glibmm/convert.h>
#include "MAparser/maparser.h"

#include "rparser.h"
#include "common.h"

using namespace xmlpp;
using namespace std;
using Glib::ustring;

Rule_sax::Rule_sax():SaxParser(){
    mapForSwitch["rule"]=rule;
    mapForSwitch["discipline"]=discipline;
    mapForSwitch["style"]=style;
    mapForSwitch["add_expression"]=add_expression;
    mapForSwitch["sex"]=sex;
    mapForSwitch["max_starting_persons"]=max_starting_persons;
    mapForSwitch["max_counted_persons"]=max_counted_persons;
    mapForSwitch["birth_min"]=birth_min;
    mapForSwitch["birth_max"]=birth_max;
    mapForSwitch["class_min"]=class_min;
    mapForSwitch["class_max"]=class_max;
    mapForSwitch["point_expression"]=point_expression;
}

void Rule_sax::on_start_document(){returned=vector<Rule>();}

void Rule_sax::on_end_document(){;}

void Rule_sax::on_start_element(const ustring& name, const AttributeList& properties){
    bool s;
    unsigned short msp=0, mcp=0;
    unsigned short bmn=0, bmx=0;
    unsigned short cmn=0, cmx=0;
    bool Rule_flag=0;
    
/******************Default values*****************/
    for(vector<AttributeList>::const_iterator attr_iter = attributes.begin(); attr_iter != attributes.end(); ++attr_iter){
        for(AttributeList::const_iterator iter = attr_iter->begin(); iter != attr_iter->end(); ++iter){
            int val;
            try{
                numConvert(val,iter->value,std::dec);
            }
            catch(bool er){
                continue;
            }
            catch(const Glib::ConvertError& ex){
                continue;
            }
                switch(mapForSwitch[iter->name]){
                    case sex: s=val; break;
                    case max_starting_persons: msp=val; break;
                    case max_counted_persons: mcp=val; break;
                    case birth_min: bmx=val; break;
                    case birth_max: bmn=val; break;
                    case class_min: cmx=val; break;
                    case class_max: cmn=val; break;
                }
        }
    }
    for(vector<ustring>::const_iterator iter=names.begin(); iter!=names.end(); ++iter){
        if(*iter=="rule")Rule_flag=1;
    }
/******************\Default values*****************/

    switch(mapForSwitch[name]){
        case rule:
            if(!Rule_flag){
                mTree ae;
                for(AttributeList::const_iterator iter = properties.begin(); iter != properties.end(); ++iter){
                    int val;
                    try{
                        numConvert(val,iter->value,std::dec);
                    }
                    catch(bool er){
                        switch(mapForSwitch[iter->name]){
                            case add_expression:
                                try{
                                    ae=mParse(iter->value);
                                }
                                catch(bool& ex){
                                    ae=mParse("x+y");
                                }
                                break;
                        }
                        continue;
                    }
                    catch(const Glib::ConvertError& ex){
                        continue;
                    }
                    switch(mapForSwitch[iter->name]){
                        case sex: s=val; break;
                        case max_starting_persons: msp=val; break;
                        case birth_min: bmx=val; break;
                        case birth_max: bmn=val; break;
                        case class_min: cmx=val; break;
                        case class_max: cmn=val; break;
                    }
                }
                returned.push_back(Rule(s, ae, msp, bmn, bmx, cmn, cmx));
                names.push_back(name);
                attributes.push_back(properties);
            }
            break;
        case discipline:
            if(Rule_flag){
                mTree pe;
                ustring dc;
                //vector<unsigned short> args;
                for(AttributeList::const_iterator iter = properties.begin(); iter != properties.end(); ++iter){
                    int val;
                    try{
                        numConvert(val,iter->value,std::dec);
                    }
                    catch(bool er){
                        switch(mapForSwitch[iter->name]){
                            case point_expression:
                                try{
                                    pe=mParse(iter->value);
                                }
                                catch(bool& ex){
                                    pe=mParse("x");
                                }
                                break;
                            case code:
                                dc=iter->value;
                                break;
                        }
                        continue;
                    }
                    catch(const Glib::ConvertError& ex){
                        continue;
                    }
                    switch(mapForSwitch[iter->name]){
                        case sex: s=val; break;
                        case max_starting_persons: msp=val; break;
                        case max_counted_persons: msp=val; break;
                        case birth_min: bmx=val; break;
                        case birth_max: bmn=val; break;
                        case class_min: cmx=val; break;
                        case class_max: cmn=val; break;
                    }
                }
                returned.end()->addDiscipline(Discipline(dc, pe, mcp, s, msp, bmn, bmx, cmn, cmx));
            }
            break;
        case style:
            names.push_back(name);
            attributes.push_back(properties);
            break;
        default:
            break;
    }
}

void Rule_sax::on_end_element(const ustring& name){
    if(name==names.back()){
        names.pop_back();
        attributes.pop_back();
    }
}

void Rule_sax::on_characters(const ustring& characters){;}

void Rule_sax::on_comment(const ustring& text){;}

void Rule_sax::on_warning(const Glib::ustring& text){
    try{
        std::cout << "on_warning(): " << text << std::endl;
    }
    catch(const Glib::ConvertError& ex){
        std::cerr << "Rule_sax::on_warning(): Exception caught while converting text for std::cout: " << ex.what() << std::endl;
    }
}

void Rule_sax::on_error(const Glib::ustring& text){
    try{
        std::cout << "on_error(): " << text << std::endl;
    }
    catch(const Glib::ConvertError& ex)
    {
        std::cerr << "Rule_sax::on_error(): Exception caught while converting text for std::cout: " << ex.what() << std::endl;
    }
}

void Rule_sax::on_fatal_error(const Glib::ustring& text){
    try{
        std::cout << "on_fatal_error(): " << text << std::endl;
    }
    catch(const Glib::ConvertError& ex){
        std::cerr << "Rule_sax::on_characters(): Exception caught while converting value for std::cout: " << ex.what() << std::endl;
    }
}

Rule_sax::~Rule_sax(){;}
