/** Notebook data - managing tabs,
	Dialogs inside tabs (incl. for
	managing persons - those call
	funcs from persons.h
	Adam Matouŝek is babysitting
	this file
**/

#include "notebook.h"
#include "tabs.h"
#include "persons.h"
#include "gui.h"
#include <iostream>

set <BaseTab*> tabs;
map <string, LocalTab*> local_tabs;

// BASE TAB

void BaseTab::show_tab(){
	label_box = new Box(ORIENTATION_HORIZONTAL, 3);
	l_label = new Label(label->c_str());
	l_label->set_use_markup();
	label_box->pack_start(*l_label);
	i_close = new Image(Stock::CLOSE, IconSize(ICON_SIZE_MENU ));
	b_close = new Button();
	b_close->add(*i_close);
	b_close->set_relief(RELIEF_NONE);
	label_box->pack_start(*b_close);
	b_close->signal_clicked().connect(sigc::mem_fun(*this, &BaseTab::self_remove));
	ntbTeams->append_page(*content, *label_box);
	label_box->show_all_children();
	ntbTeams->show_all_children();
	ntbTeams->set_current_page(-1);
	ntbTeams->set_show_tabs();
	ntbTeams->remove_page(*welcome_tab_content);
}

BaseTab::~BaseTab(){
	ntbTeams->remove_page(*content);
	tabs.erase(this);
	delete content;
	delete label;
	delete b_close;
	delete i_close;
	delete l_label;
	delete label_box;
}

void BaseTab::self_remove(){
	delete this;
}

void BaseTab::expose(){
	ntbTeams->set_current_page(ntbTeams->page_num(*content));
}

// TEST TAB

TestTab::TestTab(){
	content = new Image(Stock::OK, IconSize(ICON_SIZE_LARGE_TOOLBAR));
	label = new ustring("Nadpísek");
	show_tab();
}

TestTab::~TestTab(){
	std::cout << tabs.size() << '\n';
}

void AddTestTab(){
	tabs.insert(new TestTab());
}

// LOCAL TAB

LocalTab::LocalTab(const ustring& name){
	try{
	tab_file = Builder::create_from_file("tab-local.glade");
	} catch(const Glib::FileError& ex) {
		std::cerr << ex.what() << std::endl;
	} catch(const Gtk::BuilderError& ex) {
		std::cerr << ex.what() << std::endl;
	}
	tab_file->get_widget("box_Team", content);
	Container* tmp;
	tab_file->get_widget("tb_Inline", tmp);
	tmp->get_style_context()->add_class("inline-toolbar");
	Button* btmp;
	tab_file->get_widget("delete_team", btmp);
	btmp->signal_clicked().connect(sigc::bind(sigc::ptr_fun(DeleteLocalTeam), name));
	ToolButton *tbtmp;
	tab_file->get_widget("tbt_new", tbtmp);
	tbtmp->signal_clicked().connect(sigc::bind(sigc::ptr_fun(AddPersonDirectlyToTeam), name.c_str()));
	tab_file->get_widget("tbt_delete", tbtmp);
	tbtmp->signal_clicked().connect(sigc::mem_fun(*this, &LocalTab::button_remove_persons));
	tab_file->get_widget("tbt_add", tbtmp);
	tbtmp->signal_clicked().connect(sigc::bind(sigc::ptr_fun(SelectPersonsToTeam), name.c_str()));
	tab_file->get_widget("tbt_edit", tbtmp);
	tbtmp->signal_clicked().connect(sigc::mem_fun(*this, &LocalTab::edit_person));
	Entry* etmp;
	tab_file->get_widget("entry_new_name", etmp);
	etmp->signal_activate().connect(sigc::mem_fun(*this, &LocalTab::rename_from_entry));
	//etmp->signal_icon_press().connect(mem_fun(*this, &LocalTab::rename_from_entry));
	label = new ustring(name);
	filter = TreeModelFilter::create(persons_model);
	filter->set_visible_func(sigc::mem_fun(*this, &LocalTab::filter_func));
	tview = new TreeView(filter);
	tab_file->get_widget("tviewspace", tmp);
	tview->append_column("ID (nepobrazí se)", columns->p_id);
	tview->append_column("Jméno", columns->p_name);
	tview->set_search_column(columns->p_name);
	tview->set_visible(true);
	tview->get_selection()->set_mode(SELECTION_SINGLE);
	tmp->add(*tview);
	show_tab();
}

bool LocalTab::filter_func(const TreeModel::const_iterator& row){
	std::cout << label->c_str() << " hledá ID " << (*row)[columns->p_id] << "\n";
	return (local_teams[label->c_str()]->second.count((*row)[columns->p_id]));
}

void LocalTab::rename_from_entry(){
	Entry* etmp;
	tab_file->get_widget("entry_new_name", etmp);
	if(local_teams.count(etmp->get_text().raw())) return;
	RenameLocalTeam(*label, etmp->get_text());
}

void LocalTab::remove_person(const TreeModel::iterator& i){
	RemovePersonFromTeam(label->c_str(), (*i)[columns->p_id]);
}

void LocalTab::button_remove_persons(){
	tview->get_selection()->selected_foreach_iter(sigc::mem_fun(*this, &LocalTab::remove_person));
}

void LocalTab::edit_person(){
	if(tview->get_selection()->count_selected_rows() == 0) return;
	TreeIter i = tview->get_selection()->get_selected();
	EditPerson((*i)[columns->p_id]);
}

LocalTab::~LocalTab(){
	local_tabs.erase(label->c_str());
	delete tview;
}

void LocalTab::Refilter(){filter->refilter();}

void AddLocalTab(const ustring& name){
	if(!local_tabs.count(name.c_str())){
		local_tabs[name.c_str()] = new LocalTab(name);
		tabs.insert(local_tabs[name.c_str()]);
	}
	local_tabs[name.c_str()]->expose();
}

void CloseLocalTab(const char* name){
	local_tabs[name]->self_remove();
	local_tabs.erase(name);
}


// SUMMARY TAB

SummaryTab* opened_summary = 0; //Currently opened SummaryTab or zero

SummaryTab::SummaryTab(){
	try{
	tab_file = Builder::create_from_file("tab-summary.glade");
	} catch(const Glib::FileError& ex) {
		std::cerr << ex.what() << std::endl;
	} catch(const Gtk::BuilderError& ex) {
		std::cerr << ex.what() << std::endl;
	}
	tab_file->get_widget("box_Team", content);
	Container* tmp;
	tab_file->get_widget("tb_Inline", tmp);
	tmp->get_style_context()->add_class("inline-toolbar");
	ToolButton *tbtmp;
	tab_file->get_widget("tbt_delete", tbtmp);
	tbtmp->signal_clicked().connect(sigc::mem_fun(*this, &SummaryTab::button_remove_persons));
	tab_file->get_widget("tbt_add", tbtmp);
	tbtmp->signal_clicked().connect(sigc::ptr_fun(AddPerson));
	tab_file->get_widget("tbt_edit", tbtmp);
	tbtmp->signal_clicked().connect(sigc::mem_fun(*this, &SummaryTab::edit_person));

	label = new ustring("Souhrn");
	tview = new TreeView(persons_model);
	tab_file->get_widget("tviewspace", tmp);
	tview->append_column("ID (nepobrazí se)", columns->p_id);
	tview->append_column("Jméno", columns->p_name);
	tview->set_search_column(columns->p_name);
	tview->set_visible(true);
	tview->get_selection()->set_mode(SELECTION_SINGLE);
	tmp->add(*tview);
	show_tab();
}

void SummaryTab::remove_person(const TreeModel::iterator& i){
	storage.DelPerson((*i)[columns->p_id]);
}

void SummaryTab::button_remove_persons(){
	tview->get_selection()->selected_foreach_iter(sigc::mem_fun(*this, &SummaryTab::remove_person));
}

void SummaryTab::edit_person(){
	if(tview->get_selection()->count_selected_rows() == 0) return;
	TreeIter i = tview->get_selection()->get_selected();
	EditPerson((*i)[columns->p_id]);
}

SummaryTab::~SummaryTab(){
	opened_summary = 0;
	delete tview;
}

void AddSummaryTab(){
	if(!opened_summary)
		opened_summary = new SummaryTab();
	opened_summary->expose();
}


// OUTPUT TAB

OutputTab::OutputTab(){
	try{
	tab_file = Builder::create_from_file("tab-output.glade");
	} catch(const Glib::FileError& ex) {
		std::cerr << ex.what() << std::endl;
	} catch(const Gtk::BuilderError& ex) {
		std::cerr << ex.what() << std::endl;
	}
	tab_file->get_widget("box_Team", content);
	Widget* tmp;
	tab_file->get_widget("tb_Inline", tmp);
	tmp->get_style_context()->add_class("inline-toolbar");
	label = new ustring("Výstupní tým");
	show_tab();
}

OutputTab::~OutputTab(){
	std::cout << tabs.size() << '\n';
}

void AddOutputTab(){
	tabs.insert(new OutputTab());
}
